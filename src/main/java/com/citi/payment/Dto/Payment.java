package com.citi.payment.Dto;

import java.util.Date;

public class Payment {

	private int id;
	private Date paymentdate;
	private String type;
	private int custid;
	private double amount;
	
	/*
	 * default constructor
	 */
	public Payment() {
		
	}
	
	@Override
	public String toString() {
		return "Payment [id=" + id + ", paymentdate=" + paymentdate + ", type=" + type + ", custid=" + custid
				+ ", amount=" + amount + "]";
	}


	public int getId() {
		return id;
	}
	public Payment(int id, Date paymentdate, String type, int custid, double amount) {
		super();
		this.id = id;
		this.paymentdate = paymentdate;
		this.type = type;
		this.custid = custid;
		this.amount = amount;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(Date paymentdate) {
		
		this.paymentdate= paymentdate;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getCustid() {
		return custid;
	}
	public void setCustid(int custid) {
		this.custid = custid;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
