package com.citi.payment.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.payment.Dao.PaymentDao;
import com.citi.payment.Dto.Payment;


@Component
public class PaymentBs {

	@Autowired
	PaymentDao paymentDao;

	public int rowCount() {

		return paymentDao.rowCount();
	}

	public Payment findbyId(int id) {

		if (id == 0 || id < 0) {
			throw new IllegalArgumentException();
		}
		return paymentDao.findbyId(id);
	}

	public List<Payment> findByType() {
		
		Payment payment = new Payment();
		if (payment.getType().equals(null)) {
			throw new IllegalArgumentException();
		}
		return paymentDao.findByType();
	}

	public int save(Payment payment) {
		return paymentDao.save(payment);
		
	}
	
}
