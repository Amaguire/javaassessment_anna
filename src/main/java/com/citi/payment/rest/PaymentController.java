package com.citi.payment.rest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.payment.Dto.Payment;
import com.citi.payment.business.PaymentBs;

@RestController
@RequestMapping("/payment")

public class PaymentController {

	private static final Logger logger = LogManager.getLogger(PaymentController.class);
	@Autowired
	PaymentBs paymentBs;

	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public String getStatus() {
		logger.info("Payment Controller started");
		return "Payment Controller running";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public int save(@RequestBody Payment payment) {
		return paymentBs.save(payment);
	}

	@RequestMapping(value = "/bytype/{type}", method = RequestMethod.GET)
	public List<Payment> findByType(@PathVariable String type) {
		return paymentBs.findByType();
	}

	@RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
	public Payment findById( @PathVariable int id) {
		return paymentBs.findbyId(id);
	}
	
}
