package com.citi.payment.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.payment.Dto.Payment;

@Component

public class PaymentDao {
	private static final Logger logger = LogManager.getLogger(PaymentDao.class);

	@Autowired
	private JdbcTemplate tpl;

	public PaymentDao() {

	}

	public int rowCount() {
		return tpl.queryForObject("select count(*) from payment", Integer.class);
	}

	public Payment findbyId(int id) {
		return tpl.queryForObject("Select * from Payment where payment_id=?",
				new Object[] { id },
				new PaymentMapper());
	}

	public List<Payment> findByType() {
		logger.info("Find all invoked");
		return tpl.query("Select * from Payment", new PaymentMapper());
	}

	private static final class PaymentMapper implements RowMapper<Payment> {
		public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
			Payment payment = new 
					Payment(rs.getInt("payment_id"),
					rs.getDate("payment_date"),
					rs.getString("payment_type"), 
					rs.getInt("customer_id"), 
					rs.getDouble("amount"));
			return payment;
		}

	}

	public int save(Payment payment) {

		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						"Insert into payment(payment_id, payment_date, payment_type,customer_id,amount) VALUES (?,?,?,?,?)",

						Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, payment.getId());
				ps.setDate(2, (java.sql.Date) payment.getPaymentdate());
				ps.setString(3, payment.getType());
				ps.setInt(4, payment.getCustid());
				ps.setDouble(5, payment.getAmount());
				;

				return ps;
			}
		},

				keyHolder);
		// return 0;
		return keyHolder.getKey().intValue();

	}
}