package com.citi.payment.Dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;

import com.citi.payment.Dto.Payment;

@RunWith(SpringRunner.class)
@SpringBootTest

public class PaymentDaoIT {

	@Autowired
	PaymentDao paymentDao;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void PaymentDao_CheckRowCount_ReturnsOver0() {

		assertTrue(paymentDao.rowCount() > 0);
	}

	@Test
	public void PaymentDao_CheckFindById_ReturnsAnId() {

		Payment payment = paymentDao.findbyId(3);
		assertTrue(payment.getId()== 3);

	}
	
	 @Test
	 public void PaymentDao_CheckFindByType_ReturnsAll() {
	
	 List<Payment> payment= paymentDao.findByType(); assertTrue(payment.size()==
	 paymentDao.rowCount()); 
	 }
	 

	@Test
	public void PaymentDao_SavePayment_ReturnsPayment() {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse("2017-12-13");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		java.sql.Date sqldate = new java.sql.Date(date.getTime());
	
		int result= paymentDao.save(new Payment(145, sqldate, "visa", 4, 4.44));
			Payment payment = paymentDao.findbyId(result);

		assertTrue(payment.getId()==result);
		// delete and record after
	}
}
