package com.citi.payment.rest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;



@RunWith(SpringRunner.class)
@SpringBootTest

public class PaymentRestIT {
	
	RestTemplate restTemplate = new RestTemplate();
	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void PaymentController_checkStatus_ReturnsRunning() {
		//properties to do 
		String url 
		="http://localhost:8082/payment/status";
		ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
		assertTrue(response.getBody().equals("Payment Controller running"));
	}

}
