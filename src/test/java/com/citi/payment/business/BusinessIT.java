package com.citi.payment.business;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;

import com.citi.payment.Dto.Payment;
import com.citi.payment.business.PaymentBs;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessIT {
	@Autowired
	PaymentBs paymentBs;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void PaymentBs_InvalidIdLength_ReturnsIllegalArguementException() throws IllegalAccessException {
		paymentBs.findbyId(0);}
	

	@Test
	public void PaymentBs_ValidType_ReturnCustomer() {
		
		Payment payment= paymentBs.findbyId(4);
	assertThat(payment.getType(), is(not(nullValue())));
	}
	
	@Test
	public void PaymentBs_SavePayment_ReturnsPayment() {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse("2017-12-13");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		java.sql.Date sqldate = new java.sql.Date(date.getTime());
	
	//paymentBs.save(new Payment(11100, sqldate, "visa", 4, 4.44));
			Payment payment = paymentBs.findbyId(11100);
			
		assertTrue(payment.getType().equals("visa"));
		// delete and record after
	}
}
